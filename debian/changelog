straight.plugin (1.4.1-7) unstable; urgency=medium

  * Team upload.
  * Remove usage of python3-mock

 -- Alexandre Detiste <tchet@debian.org>  Sun, 15 Dec 2024 01:40:37 +0100

straight.plugin (1.4.1-6) unstable; urgency=medium

  * Team upload.
  * Run tests using pytest (closes: #1080287).
  * Use pybuild-plugin-pyproject.

 -- Colin Watson <cjwatson@debian.org>  Sun, 08 Sep 2024 18:28:46 +0100

straight.plugin (1.4.1-5) unstable; urgency=medium

  [ Yogeswaran Umasankar ]
  * Team upload.
  * Added patch for straight/plugin/loaders.py and tests.py to
    replace deprecated imp with importlib to fix ModuleNotFoundError
    in Python 3.12. (Closes: #1058197)
  * Added d/tests for autopkgtest.
  * Updated Standards-Version to 4.6.2 in d/control.

  [ Andreas Tille ]
  * Fix clean target
    Closes: #1047731
  * debhelper-compat 13 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)
  * Remove trailing whitespace in debian/changelog (routine-update)
  * Rules-Requires-Root: no (routine-update)

 -- Andreas Tille <tille@debian.org>  Sun, 14 Jan 2024 21:36:47 +0100

straight.plugin (1.4.1-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Apply multi-arch hints.
    + python-straight.plugin-doc: Add Multi-Arch: foreign.

 -- Sandro Tosi <morph@debian.org>  Sat, 04 Jun 2022 20:53:22 -0400

straight.plugin (1.4.1-3) unstable; urgency=medium

  * Team upload.
  * d/control: Set Vcs-* to salsa.debian.org
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Drop Python 2 support.

 -- Ondřej Nový <onovy@debian.org>  Fri, 26 Jul 2019 19:26:22 +0200

straight.plugin (1.4.1-2) unstable; urgency=medium

  * Fixing lintian warnings.
    Removing starting article ("A") and the trailing "for Python" text
    from description synopsis.

 -- Sergio Durigan Junior <sergiodj@sergiodj.net>  Sun, 10 Jul 2016 15:36:56 -0400

straight.plugin (1.4.1-1) unstable; urgency=medium

  * Initial release of straight.plugin. (Closes: #829153)

 -- Sergio Durigan Junior <sergiodj@sergiodj.net>  Fri, 01 Jul 2016 18:40:42 -0400
